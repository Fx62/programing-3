/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package febrero16;

/**
 *
 * @author fx62
 */
public class Recursivity {

    private static int[] list = {22, 12, 3, 11, 93, 54, 32, 6, 5};

    public static void main(String[] args) {
        String binario = "1011000";
        System.out.println("Binario: " + binario + " = " + conversion("1011000"));
        System.out.println("El número mas pequeño de la lista es: " + menor());
        if(args.length != 0){
            // IDE variable for arguments is -> application.args=2236
            System.out.println("La suma de cada dígito de " + args[0] + " es: " + suma(args[0]));
        } else{
            System.out.println("Argumento no especificado");
        }

    }

    public static int conversion(String bin) {
        int size = bin.length();
        if (size == 1) {
            return Integer.parseInt(bin);
        } else {
            return conversion(bin.substring(1)) + conversion(String.valueOf(bin.charAt(0))) * (int) Math.pow(2, size - 1);
        }
    }

    public static int menor() {
        if (list.length == 0) {
            System.out.println("Lista vacia");
            return 0;
        } else if (list.length == 1) {
            return menor(list[0], 0);
        } else {
            return menor(list[0], 1);
        }
    }

    public static int menor(int num, int position) {
        if (num > list[position]) {
            num = list[position];
        }
        //System.out.println(position + "    " + list.length);
        if (list.length - 1 != position) {
            position += 1;
            num = menor(num, position);
        }
        return num;
    }
    
    public static int suma(String args){
        //System.out.println("* " + args);
        int size = args.length();
        int value = 0;
        if(args.length() == 1){
            return Integer.parseInt(args);
        } else{
            value += suma(args.substring(1)) + suma(args.substring(0, 1));
        }
        return value;
    }
}
