package chess;

import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author fx62
 */
public class Game {

    private static final String[][] matrix = new String[8][8];

    public static void main(String[] args) {
        matrix[7][0] = matrix[7][7] = "Torre";
        matrix[7][1] = matrix[7][6] = "Caballo";
        matrix[7][2] = matrix[7][5] = "Alfil";
        matrix[7][3] = "Reina";
        matrix[7][4] = "Rey";
        matrix[6][0] = matrix[6][1] = matrix[6][2] = matrix[6][3]
             = matrix[6][4] = matrix[6][5] = matrix[6][6] = matrix[6][7] = "Peon";
        show();
        Scanner scanner = new Scanner(System.in);
        boolean control = true;
        while (control) {
            try {
                System.out.println("\nPieza a mover");
                System.out.print("Fila actual: ");
                byte filaActual = scanner.nextByte();
                System.out.print("Coumna actual: ");
                byte columnaActual = scanner.nextByte();
                if (filaActual >= 0 && filaActual <= 7 && columnaActual >= 0 && columnaActual <= 7) {
                    if (matrix[filaActual][columnaActual] != null) {
                        System.out.println("Se puede mover");
                        System.out.println("Nueva posicion");
                        System.out.print("Fila a mover: ");
                        byte filaFutura = scanner.nextByte();
                        System.out.print("Columna a mover: ");
                        byte columnaFutura = scanner.nextByte();
                        if (filaFutura >= 0 && filaFutura <= 7 && columnaFutura >= 0 && columnaFutura <= 7) {
                            if (matrix[filaFutura][columnaFutura] == null) {
                                switch (matrix[filaActual][columnaActual]) {
                                    case "Peon":
                                        System.out.println(moverPeon(filaActual, columnaActual, filaFutura, columnaFutura)
                                                ? "Pieza movida" : "Movimiento invalido");
                                        break;
                                    case "Torre":
                                        System.out.println(moverTorre(filaActual, columnaActual, filaFutura, columnaFutura)
                                                ? "Pieza movida" : "Movimiento invalido");
                                        break;
                                    case "Caballo":
                                        System.out.println(moverCaballo(filaActual, columnaActual, filaFutura, columnaFutura)
                                                ? "Pieza movida" : "Movimiento invalido");
                                        break;
                                    case "Alfil":
                                        System.out.println(moverAlfil(filaActual, columnaActual, filaFutura, columnaFutura)
                                                ? "Pieza movida" : "Movimiento invalido");
                                        break;
                                    case "Reina":
                                        System.out.println(moverReina(filaActual, columnaActual, filaFutura, columnaFutura)
                                                ? "Pieza movida" : "Movimiento invalido");
                                        break;
                                    case "Rey":
                                        System.out.println(moverRey(filaActual, columnaActual, filaFutura, columnaFutura)
                                                ? "Pieza movida" : "Movimiento invalido");
                                        break;
                                }
                                show();
                            } else {
                                System.out.println("La casilla especificada esta ocupada");
                            }
                        } else {
                            System.out.println("El rango ingresado es mayor al del tablero");
                        }
                    } else {
                        System.out.println("Esta casilla esta vacia");
                    }
                } else {
                    System.out.println("El rango ingresado es mayor al del tablero");
                }
            } catch (Exception e) {
                control = false;
            }
        }
    }

    private static boolean moverPeon(byte filaActual, byte columnaActual, byte filaFutura, byte columnaFutura) {
        boolean control = false;
        if (columnaActual == columnaFutura) {
            if (filaActual == 6) {
                if (filaActual - filaFutura == 2) {
                    if (matrix[filaFutura - 1][columnaFutura] == null) {
                        control = true;
                    }
                }
            }
            if (filaActual - filaFutura == 1) {
                control = true;
            }
            if (control) {
                move(filaActual, columnaActual, filaFutura, columnaFutura);
                if (filaFutura == 0) {
                    System.out.println("El peon se corono, ahora el peon puede convertirse en reina, torre, caballo o alfil");
                }
            }
        }

        return control;
    }

    private static boolean moverTorre(byte filaActual, byte columnaActual, byte filaFutura, byte columnaFutura) {
        boolean control = false;
        if (filaActual == filaFutura) {
            if (columnaActual > columnaFutura) {
                int temp = columnaFutura;
                while (temp < columnaActual) {
                    if (matrix[filaFutura][temp] != null) {
                        return false;
                    }
                    temp++;
                }
                control = true;
            } else {
                int temp = columnaActual + 1;
                while (temp <= columnaFutura) {
                    if (matrix[filaFutura][temp] != null) {
                        return false;
                    }
                    temp++;
                }
                control = true;
            }
        }
        if (columnaActual == columnaFutura) {
            if (filaActual > filaFutura) {
                int temp = filaActual;
                while (temp < filaActual) {
                    if (matrix[temp][columnaFutura] != null) {
                        return false;
                    }
                    temp++;
                }
                control = true;
            } else {
                int temp = filaActual + 1;
                while (temp <= filaFutura) {
                    if (matrix[temp][columnaFutura] != null) {
                        return false;
                    }
                    temp++;
                }
                control = true;
            }
        }
        if (control) {
            move(filaActual, columnaActual, filaFutura, columnaFutura);
        }
        return control;
    }

    private static boolean moverAlfil(byte filaActual, byte columnaActual, byte filaFutura, byte columnaFutura) {
        boolean control = false;
        if ((filaActual != filaFutura) && (columnaActual != columnaFutura)) {
            if (Math.abs(filaActual - filaFutura) == Math.abs(columnaActual - columnaFutura)) {
                //abajo
                if (filaActual - filaFutura < 0) {
                    //derecha
                    if (columnaActual - columnaFutura < 0) {
                        int filaTemp = filaActual + 1;
                        int columnaTemp = columnaActual + 1;
                        while (filaTemp < 8 || columnaTemp < 8) {
                            if (matrix[filaTemp][columnaTemp] != null) {
                                return false;
                            }
                            if (filaTemp == filaFutura && columnaTemp == columnaFutura) {
                                control = true;
                                break;
                            }
                            filaTemp++;
                            columnaTemp++;
                        }
                    } else { // izquierda
                        int filaTemp = filaActual + 1;
                        int columnaTemp = columnaActual - 1;
                        while (filaTemp < 8 || columnaTemp > -1) {
                            if (matrix[filaTemp][columnaTemp] != null) {
                                return false;
                            }
                            if (filaTemp == filaFutura && columnaTemp == columnaFutura) {
                                control = true;
                                break;
                            }
                            filaTemp++;
                            columnaTemp--;
                        }
                    }
                } else { // arriba
                    // derecha
                    if (columnaActual - columnaFutura < 0) {
                        int filaTemp = filaActual - 1;
                        int columnaTemp = columnaActual + 1;
                        while (filaTemp < 8 || columnaTemp < 8) {
                            if (matrix[filaTemp][columnaTemp] != null) {
                                return false;
                            }
                            if (filaTemp == filaFutura && columnaTemp == columnaFutura) {
                                control = true;
                                break;
                            }
                            filaTemp--;
                            columnaTemp++;
                        }
                    } else { // izquierda
                        int filaTemp = filaActual - 1;
                        int columnaTemp = columnaActual - 1;
                        while (filaTemp < 8 || columnaTemp > -1) {
                            if (matrix[filaTemp][columnaTemp] != null) {
                                return false;
                            }
                            if (filaTemp == filaFutura && columnaTemp == columnaFutura) {
                                control = true;
                                break;
                            }
                            filaTemp--;
                            columnaTemp--;
                        }
                    }
                }
            }
        }
        if (control) {
            move(filaActual, columnaActual, filaFutura, columnaFutura);
        }
        return control;
    }

    private static boolean moverCaballo(byte filaActual, byte columnaActual, byte filaFutura, byte columnaFutura) {
        boolean control = false;
        if (Math.abs(filaActual - filaFutura) == 1) {
            if (Math.abs(columnaActual - columnaFutura) == 2) {
                control = true;
            }
        } else if (Math.abs(filaActual - filaFutura) == 2) {
            if (Math.abs(columnaActual - columnaFutura) == 1) {
                control = true;
            }
        }
        if (control) {
            move(filaActual, columnaActual, filaFutura, columnaFutura);
        }
        return control;
    }

    private static boolean moverReina(byte filaActual, byte columnaActual, byte filaFutura, byte columnaFutura) {
        boolean control = false;
        if (moverTorre(filaActual, columnaActual, filaFutura, columnaFutura)
                || moverAlfil(filaActual, columnaActual, filaFutura, columnaFutura)) {
            control = true;
        }
        //if (control) {
        //move(filaActual, columnaActual, filaFutura, columnaFutura);
        //}
        return control;
    }

    private static boolean moverRey(byte filaActual, byte columnaActual, byte filaFutura, byte columnaFutura) {
        boolean control = false;
        if (filaActual == filaFutura) {
            if (Math.abs(columnaActual - columnaFutura) == 1) {
                control = true;
            }
        }
        if (columnaActual == columnaFutura) {
            if (Math.abs(filaActual - filaFutura) == 1) {
                control = true;
            }
        }
        if (Math.abs(filaActual - filaFutura) == 1 && Math.abs(columnaActual - columnaFutura) == 1) {
            control = true;
        }
        if (control) {
            move(filaActual, columnaActual, filaFutura, columnaFutura);
        }
        return control;
    }

    public static void show() {
        for (byte k = 0; k < 8; k++) {
            System.out.print("\t" + k);
        }
        System.out.println();
        for (byte i = 0; i < matrix.length; i++) {
            System.out.print(i + "\t");
            for (byte j = 0; j < matrix[i].length; j++) {
                if (i == 0) {
                }
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static void move(byte filaActual, byte columnaActual, byte filaFutura, byte columnaFutura) {
        matrix[filaFutura][columnaFutura] = matrix[filaActual][columnaActual];
        matrix[filaActual][columnaActual] = null;
    }
}
