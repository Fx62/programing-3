package febrero2_9;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fx62
 */
public class Nodo {
    private Object dato;
    private Nodo siguiente;

    public Nodo(Object dato) {
        this.dato = dato;
        this.siguiente = siguiente;
    }

    public Object getDato(){
        return dato;
    }

    public void setDato(){
        this.dato = dato;
    }

    public void setSig(Nodo sig){
        this.siguiente = sig;
    }

    public Nodo getSig(){
        return siguiente;
    }
}
