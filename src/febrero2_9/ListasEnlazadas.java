package febrero2_9;

import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fx62
 */
public class ListasEnlazadas {
    private Nodo inicio;
    private Nodo ultimo;

    public Nodo getUltimo() {
        return ultimo;
    }
    
    public Nodo getInicio() {
        return inicio;
    }

    public ListasEnlazadas(){
        inicio = null;
        ultimo = null;
    }

    public boolean estaVacia(){
        return inicio == null;
    }
    
    public void agregarInicio(Object dato){
        Nodo nuevo = new Nodo(dato);
        if(!estaVacia()){
            nuevo.setSig(inicio);
        } else {
            ultimo = nuevo;
        }
        inicio = nuevo;
    }
    
    public void agregarUltimo(Object dato){
        if(estaVacia()){
            agregarInicio(dato);
        } else{
            Nodo nuevo = new Nodo(dato);
            ultimo.setSig(nuevo);
            ultimo = nuevo;
            // int a = 2;;
        }
    }
    
    public void eliminarUltimo(){
        if(inicio == ultimo && inicio!= null){
            inicio=ultimo=null;
        }
        if(!estaVacia()){
            ultimo = devolverPenultimo();
            ultimo.setSig(null);
        }
    }
    
    public String mostrarRecursivo(Nodo nodo){
        if(nodo.getSig() == null){
            return nodo.getDato() + " -> null";
        } else{
            return nodo.getDato() + " -> " + mostrarRecursivo(nodo.getSig());
        }
    }
    
    public Nodo devolverPenultimo(){
        Nodo temp = inicio;
        while(temp.getSig() != ultimo){
            temp=temp.getSig();
        }
        return temp;
    }

    public void recorrer(){
        Nodo temp = inicio;
        while(temp != null){
            System.out.print(temp.getDato() + " -> ");
            temp=temp.getSig();
        }
        System.out.println();
    }

    public static void main(String[] args){
        ListasEnlazadas lista1 = new ListasEnlazadas();
        //System.out.println(ultimo.getDato());
        lista1.agregarInicio("lol");
        //System.out.println(ultimo.getDato());
        lista1.agregarInicio(new Integer(2));
        //System.out.println(ultimo.getDato());
        lista1.agregarInicio(new Date());
        //System.out.println(ultimo.getDato());
        //lista1.recorrer();
        //lista1.eliminar();
        System.out.println(lista1.mostrarRecursivo(lista1.getInicio()));
//        lista1.agregarUltimo(50);
//        lista1.recorrer();
//        lista1.eliminarUltimo();
//        lista1.eliminarUltimo();
//        lista1.recorrer();
//        lista1.eliminar();
//        lista1.recorrer();
//        lista1.eliminar();
//        lista1.recorrer();
    }

    public void eliminar(){
        if(!estaVacia()){
            inicio = inicio.getSig();
        }
        if (inicio == null){
            ultimo = inicio;
        }
    }
}
