/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marzo23;

/**
 *
 * @author fx62
 */
public class Node {
    private Node left;
    private Node right;
    private Object data;
    public Node(Object data){
        left = right = null;
        this.data = data;
    }
    
    public Node(Node left, Node right, Object data){
        this.left = left;
        this.right = right;
        this.data = data;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
