/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marzo23;

/**
 *
 * @author fx62
 */
public class BinarySearchTree {
    private Node root;
    
    public BinarySearchTree(){
        this.root = null;
    }
    
    public BinarySearchTree(Node root){
        this.root = root;
    }
    
    public boolean isEmpty(){
        return root == null;
    }
    
    public void add(Object value) throws Exception{
        Compare data = (Compare)value;
        root = add(root, data);
    }
    
    public Node add(Node subRoot, Compare data) throws Exception{
        if(subRoot == null){
            subRoot = new Node(data);
        } else if(data.lessThan(subRoot.getData())){
            Node left = add(subRoot.getLeft(), data);
            subRoot.setLeft(left);
        } else if(data.greaterThan(subRoot.getData())){
            Node right = add(subRoot.getRight(), data);
            subRoot.setRight(right);
        } else{
            throw new Exception("This node exists");
        }
        return subRoot;
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }
}
