/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marzo23;

import java.util.Objects;

/**
 *
 * @author fx62
 */
public class Student implements Compare{
    private int id;
    private String name;

    public Student(int id, String name){
        this.id = id;
        this.name = name;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean isEqual(Object q) {
        Student a = (Student)q;
        return this.id == a.id;
    }

    @Override
    public boolean lessThan(Object q) {
        Student a = (Student)q;
        return this.id < a.id;
    }

    @Override
    public boolean greaterThan(Object q) {
        Student a = (Student)q;
        return this.id > a.id;
    }
    
}
