/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marzo23;

/**
 *
 * @author fx62
 */
public interface Compare {
    boolean isEqual(Object q);
    boolean lessThan(Object q);
    boolean greaterThan(Object q);
}
